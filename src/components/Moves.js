function MovesList(props) {
  const moves = props.moves;
  const listItems = moves.map((move, index) => 
    <li key={index}>{move}</li>
  );
  return (
    <div className='MovesList'>
      <h3>Moves:</h3>
      <ol>{listItems}</ol>
    </div>
  )
}

export default MovesList;