import React from 'react';
import Chess from 'chess.js';
import Chessboard from 'chessboardjsx';

// Cal's AI agent
import getMove from '../agent.js';
import MovesList from './Moves.js';
import './Game.css'


class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 'New Game',
      chess: new Chess(),
      position: 'start', 
      formValue: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  doMove(move) {
    if (this.state.status === 'New Game') {
      this.setState({status: 'Game In Progress'});
    }
    // check if move is valid
    let chess = this.state.chess;
    if (!(chess.moves().includes(move))) {
      alert('Invalid move: ' + move);
       return;  
    }
    chess.move(move);
    this.setState({
      position: chess.fen(),
      formValue: '',
    });

    // AI's turn:
    // let aiMove = getMove(this.state.chess.board());
    // this.state.chess.move(aiMove);
    // this.setState({
    //   position: chess.fen()
    // });
  }

  handleChange(event) {
    this.setState({formValue: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.doMove(this.state.formValue);
  }

  

  render() {
    return (
      <div className='Game'>
        <div id='game__status'>{this.state.status}</div>
        <div id='game__form'>
          <form onSubmit={this.handleSubmit}>
            <label>
              Move:
              <input type="text" value={this.state.formValue} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Submit" class='btn' />
          </form>
        </div>
        <div id='game__board'>
          <Chessboard position={this.state.position}></Chessboard>
          <div id='moves'>
            <MovesList moves={this.state.chess.history()} />
          </div>
        </div>
        
      </div>
    )
  }
}

export default Game;

