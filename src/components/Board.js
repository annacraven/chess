import React from 'react';

import './Board.css';
import Square from './Square.js';

const Col = [
  'a','b','c','d','e','f','g','h'
];

class Board extends React.Component {
  squareId(row, col) {
    return `${Col[col]}${8 - row}`;
  }
  getValue(row, col) {
    return this.props.board[row][col];
    // let square = this.props.board[row][col];
    // return square ? square.type : null;
  }
  renderSquare(row, col) {
    return (
      <Square
        dark={ (row + col) % 2 }
        value={ this.getValue(row,col) }
        id={ this.squareId(row,col) }
        key={ this.squareId(row, col) }
      />
    )
  }
  
  render() {
    let rows = [];
    for (let i = 0; i < 8; i++) {
      let row = [];
      for (let j = 0; j < 8; j++) {
        row.push(
          this.renderSquare(i,j)
        );
      }
      rows.push(row);
    }
    rows = rows.map((row) => {
      return (
        <div className='board-row'>
          { row }
        </div>
      )
    });
    return (
      <div className='Board'>
        { rows }
      </div>
    )
  }
}

export default Board;
