import './Square.css';

function Square(props) {
    return (
      <button className={`Square ${props.dark ? 'dark' : 'light'}`}>
        { props.value?.type }
      </button>
    );
  }
  
export default Square;