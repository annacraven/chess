import './App.css';
import Game from './components/Game.js';


function App() {
  return (
    <div className="App">
      <h1>A Chess App to Learn React</h1>
      <Game />
    </div>
  );
}

export default App;
